class DimensionsOutOfBoundError(Exception):
    def __init__(self, text):
        self.txt = text


class Package(object):

    def __init__(self, length = 0, width = 0, height= 0, weight= 0):
        self.length = length
        self.width = width
        self.height = height
        self.weight = weight

    @property
    def volume(self):
        return self.length * self.width * self.height

    @property
    def length(self):
        return self.le

    @length.setter
    def length(self, value):
        self.le = value
        if value < 0 or value >= 350:
            raise DimensionsOutOfBoundError(f'Package length=={value} out of bounds, should be: 0 < length <= 350')


    @property
    def width(self):
        return self.wi

    @width.setter
    def width(self, value):
        self.wi = value
        if value < 0 or value >= 300:
            raise DimensionsOutOfBoundError(f'Package width=={value} out of bounds, should be: 0 < width <= 300')

    @property
    def height(self):
        return self.he

    @height.setter
    def height(self, value):
        self.he = value
        if value < 0 or value >= 150:
            raise DimensionsOutOfBoundError(f'Package height=={value} out of bounds, should be: 0 < height <= 150')

    @property
    def weight(self):
        return self.we

    @weight.setter
    def weight(self, value):
        self.we = value
        if value < 0 or value >= 40:
            raise DimensionsOutOfBoundError(f'Package weight=={value} out of bounds, should be: 0 < weight <= 40')