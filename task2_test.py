# coding: utf-8
"""
Данный файл содержит в себе скрипт для тестирования 2 задания
 6 лабораторной работы по основам программирования

Инструкция:
s
1. создайте файл task2.py и положите его в папку(любую)
2. скопируйте данный файл в папку в которой лежит файл task2.py
3. запустите скопированный ранее файл
4. в ходе своей работы файл проведет тестирование вашей программы
 и напишет какие тесты были пройдены, а какие нет
"""


import os
import unittest
import tempfile
from random import uniform, randint
from pathlib import Path
import json
from contextlib import contextmanager


@contextmanager
def get_temp_file():
    try:
        file_ = Path(tempfile.mktemp())
        file_.touch(766)
        yield file_
    finally:
        os.remove(file_)


def create_tempfile(func):
    def wrapper(*args, **kwargs):
        with get_temp_file() as json_file:
            return func(*args, json_file, **kwargs)
    return wrapper


class Task2Test(unittest.TestCase):
    """
    Класс для тестирования 2 задания
    """

    def test_import(self):
        """
        Тестирование импорта и наличие класса
        """
        try:
            import task2 as task
            type(task.jsonattr)
        except ImportError:
            raise ImportError('Нету файла task2.py')
        except AttributeError:
            raise AttributeError('В файле task2.py нету функции jsonattr')

    @create_tempfile
    def test_make(self, json_file):
        """
        Проверка на создание класса (валидная)
        """
        from task2 import jsonattr

        json_obj = {
            "foo": "bar",
            "an_int": 5,
            "this_kata_is_awesome": True
        }

        json.dump(json_obj, json_file.open('w', encoding='utf-8'))

        @jsonattr(str(json_file))
        class TestClass:
            pass

        self.assertEqual(TestClass.foo, 'bar')
        self.assertEqual(TestClass.an_int, 5)
        self.assertEqual(TestClass.this_kata_is_awesome, True)

    @create_tempfile
    def test_bound(self, json_file):
        """
        Проверка на поведение при граничных ситуациях
        """

        from task2 import jsonattr

        class TestClass:
            pass

        json.dump({}, json_file.open('w'))

        # Проверка на пустой набор атрибутов
        symbs_list = dir(TestClass)

        symbs_list_new = dir(jsonattr(json_file)(TestClass))

        for symb in symbs_list_new:
            self.assertIn(
                symb, symbs_list, 'При пустом json файле у вас появляются новые аттрибуты')


if __name__ == "__main__":
    unittest.main()
