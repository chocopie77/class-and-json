# coding: utf-8
"""
Данный файл содержит в себе скрипт для тестирования 1 задания
 6 лабораторной работы по основам программирования

Инструкция:
s
1. создайте файл task1.py и положите его в папку(любую)
2. скопируйте данный файл в папку в которой лежит файл task1.py
3. запустите скопированный ранее файл
4. в ходе своей работы файл проведет тестирование вашей программы
 и напишет какие тесты были пройдены, а какие нет
"""


import unittest
from random import uniform, randint


class Task1Test(unittest.TestCase):
    """
    Класс для тестирования 1 задания
    """

    ARGS_NAME = ['length', 'width', 'height', 'weight']
    ARGS_BOUND = [350, 300, 150, 40]

    def test_import(self):
        """
        Тестирование импорта и наличие класса
        """
        try:
            import task1 as task
            type(task.Package)
        except ImportError:
            raise ImportError('Нету файла task1.py')
        except AttributeError:
            raise AttributeError('В файле task1.py нету класса Package')

    def test_custom_exception(self):
        """
        Тестирования исключения
        """
        try:
            import task1 as task
            e = task.DimensionsOutOfBoundError
            self.assertTrue(issubclass(
                e, Exception), '"DimensionsOutOfBoundError" не является потомком класса "Exception"')
        except ImportError:
            raise ImportError('Нету файла task1.py')
        except AttributeError:
            raise AttributeError(
                'В файле task1.py нету класса DimensionsOutOfBoundError')

    def test_make_class(self):
        """
        Тестирование класса на создание
        """
        import task1 as task

        Package = task.Package

        # Тест на 4 аргумента
        try:
            Package(0.2, 0.2, 0.2, 0.2)
        except:
            raise Exception('Ваш класс не принимает 4 аргумента')

        # Тест на 3 аргумента
        try:
            Package(0.2, 0.2, 0.2)
        except:
            raise Exception('Ваш класс не принимает 3 аргумента')

        for _ in range(1000):

            length = uniform(0, self.ARGS_BOUND[0])
            width = uniform(0, self.ARGS_BOUND[1])
            height = uniform(0, self.ARGS_BOUND[2])
            weight = uniform(0, self.ARGS_BOUND[3])
            p = Package(length, width, height, weight)

            self.assertEqual(p.length, length,
                             'Заданная длина не равна длине в классе')
            self.assertEqual(
                p.width, width, 'Заданная ширина не равна ширине в классе')
            self.assertEqual(p.height, height,
                             'Заданная высота не равна высоте в классе')
            self.assertEqual(p.weight, weight,
                             'Заданный вес не равен весу в классе')

    def test_bound_class(self):
        """
        Тестирование класса на граничные значения
        """
        import task1 as task

        Package = task.Package
        DimensionsOutOfBoundError = task.DimensionsOutOfBoundError

        for _ in range(10000):       
            try:

                Package(**{
                self.ARGS_NAME[i]: uniform(-1000, -1)
                for i in range(4)
            })
            except Exception as e:
                self.assertIsInstance(
                    e, DimensionsOutOfBoundError, 'Выбрасываемое исключение не является экземпляром класса DimensionsOutOfBoundError')
            else:
                raise Exception('Ваша программа не генерирует исключений')
        
        for _ in range(10000):       
            try:

                Package(**{
                self.ARGS_NAME[i]: uniform(self.ARGS_BOUND[i] + 1, self.ARGS_BOUND[i] + 100)
                for i in range(4)
            })
            except Exception as e:
                self.assertIsInstance(
                    e, DimensionsOutOfBoundError, 'Выбрасываемое исключение не является экземпляром класса DimensionsOutOfBoundError')
            else:
                raise Exception('Ваша программа не генерирует исключений')

    def test_exception_msg(self):
        """
        Тестирование класса на правильность вывода сообщения об ошибке
        """
        import task1 as task

        Package = task.Package
        DimensionsOutOfBoundError = task.DimensionsOutOfBoundError

        # Проверка на целое число
        try:
            Package(351, 0.2, 0.2, 0.2)
        except DimensionsOutOfBoundError as e:
            msg = str(e)
            self.assertEqual(
                msg,
                'Package length==351 out of bounds, should be: 0 < length <= 350',
                "Ваше сообщение отличается от заданного"
            )

        # Проверка на дробные числа
        def_args = [0.1, 0.2, 0.3, 5]
        for i in range(1000):
            for num_arg in range(4):
                val = uniform(-1000, -
                              350) if i % 2 == 0 else uniform(351, 1000)
                val = round(val, randint(1, 10))
                args = def_args.copy()
                args[num_arg] = val
                try:
                    Package(*args)
                except DimensionsOutOfBoundError as e:
                    self.assertEqual(
                        str(e),
                        f'Package {self.ARGS_NAME[num_arg]}=={val} out of bounds, should be: 0 < {self.ARGS_NAME[num_arg]} <= {self.ARGS_BOUND[num_arg]}'
                    )

    def test_exception_set(self):
        """
        Тестирование класса на выброс ошибки при изменении значения атрибута
        """
        import task1 as task

        Package = task.Package
        DimensionsOutOfBoundError = task.DimensionsOutOfBoundError

        # Проверка на целое число
        try:
            p = Package(0.2, 0.2, 0.2, 0.2)
            p.length = 351
        except DimensionsOutOfBoundError as e:
            msg = str(e)
            self.assertEqual(
                msg,
                'Package length==351 out of bounds, should be: 0 < length <= 350',
                "Ваше сообщение отличается от заданного"
            )

        # Проверка на дробные числа
        def_args = [0.1, 0.2, 0.3, 5]
        for i in range(1000):
            for num_arg in range(4):
                val = uniform(-1000, -
                              350) if i % 2 == 0 else uniform(351, 1000)
                val = round(val, randint(1, 10))
                try:
                    p = Package(*def_args)
                    setattr(p, self.ARGS_NAME[num_arg], val)
                except DimensionsOutOfBoundError as e:
                    msg = str(e)
                    self.assertEqual(
                        msg,
                        f'Package {self.ARGS_NAME[num_arg]}=={val} out of bounds, should be: 0 < {self.ARGS_NAME[num_arg]} <= {self.ARGS_BOUND[num_arg]}',
                        "Ваше сообщение отличается от заданного шаблона"
                    )
                else:
                    raise Exception(
                        'Ваша программа не обрабатывает ситуацию с изменением значения атрибута')
    
    def test_volume_propery(self):
        """
        Проверка вычисляемого свойства
        """
        import task1 as task

        Package = task.Package

        def calc_volume(*args):
            from functools import reduce
            return reduce(lambda a, b: a * b, args, 1)

        for _ in range(1000):
            args = [uniform(0, self.ARGS_BOUND[i]) for i in range(4)]
            p = Package(*args)
            v = round(calc_volume(*args[:-1]), 5)

            pval = round(p.volume, 5)

            self.assertEqual(v, pval, "Вычисляемое свойство 'volume' не правильно считает объем")

            
            idx_arg = randint(0, 3)
            arg_name = self.ARGS_NAME[idx_arg]
            args[idx_arg] = uniform(0, 10)

            v = round(calc_volume(*args[:-1]), 5)
            
            setattr(p, arg_name, args[idx_arg])

            pval = round(p.volume, 5)

            self.assertEqual(v, pval, "Вычисляемое свойство 'volume' не правильно считает объем при изменении параметров класса для уже существующего экземпляра Package")


if __name__ == "__main__":
    unittest.main()
