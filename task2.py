import json


def jsonattr(file):
    def wrapped(func):
        with open(file, 'r', encoding='utf-8') as f:
            data = json.loads(f.read())
            for key, value in data.items():
                setattr(func, key, value)
        return func
    return wrapped